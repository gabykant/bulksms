-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: ngiilsdh_bulksms
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `ngiilsdh_bulksms`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `ngiilsdh_bulksms` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `ngiilsdh_bulksms`;

--
-- Table structure for table `admin_menu`
--

DROP TABLE IF EXISTS `admin_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_menu`
--

LOCK TABLES `admin_menu` WRITE;
/*!40000 ALTER TABLE `admin_menu` DISABLE KEYS */;
INSERT INTO `admin_menu` VALUES (1,0,1,'Index','fa-bar-chart','/',NULL,NULL),(2,0,2,'Admin','fa-tasks','',NULL,NULL),(3,2,3,'Users','fa-users','auth/users',NULL,NULL),(4,2,4,'Roles','fa-user','auth/roles',NULL,NULL),(5,2,5,'Permission','fa-user','auth/permissions',NULL,NULL),(6,2,6,'Menu','fa-bars','auth/menu',NULL,NULL),(7,2,7,'Operation log','fa-history','auth/logs',NULL,NULL),(8,0,8,'Helpers','fa-gears','',NULL,NULL),(9,8,9,'Scaffold','fa-keyboard-o','helpers/scaffold',NULL,NULL),(10,8,10,'Database terminal','fa-database','helpers/terminal/database',NULL,NULL),(11,8,11,'Laravel artisan','fa-terminal','helpers/terminal/artisan',NULL,NULL),(12,0,0,'SMS Plans','fa-bars','/sms-plan','2017-08-13 17:21:44','2017-08-13 17:21:44'),(13,0,0,'All users','fa-users','/customers','2017-08-14 13:19:53','2017-08-14 13:19:53');
/*!40000 ALTER TABLE `admin_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_operation_log`
--

DROP TABLE IF EXISTS `admin_operation_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_operation_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_operation_log_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_operation_log`
--

LOCK TABLES `admin_operation_log` WRITE;
/*!40000 ALTER TABLE `admin_operation_log` DISABLE KEYS */;
INSERT INTO `admin_operation_log` VALUES (1,1,'admin','GET','127.0.0.1','[]','2017-08-13 17:09:25','2017-08-13 17:09:25'),(2,1,'admin/helpers/scaffold','GET','127.0.0.1','[]','2017-08-13 17:13:32','2017-08-13 17:13:32'),(3,1,'admin/helpers/scaffold','POST','127.0.0.1','{\"table_name\":\"sms_plan\",\"model_name\":\"App\\\\Models\\\\SMSPlan\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\SMSPlanController2\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"plan_name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"sms_quantity\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"validity_days\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"plan_cost\",\"type\":\"double\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"activate_plan\",\"type\":\"enum\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Di0EWnavjnhMC3c5HkLOCt3UoWsxlnTHfwjk7pdU\"}','2017-08-13 17:19:31','2017-08-13 17:19:31'),(4,1,'admin/helpers/scaffold','GET','127.0.0.1','[]','2017-08-13 17:19:32','2017-08-13 17:19:32'),(5,1,'admin/helpers/scaffold','POST','127.0.0.1','{\"table_name\":\"sms_plan\",\"model_name\":\"App\\\\Models\\\\SMSPlan\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\SMSPlanController2\",\"create\":[\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"plan_name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"sms_quantity\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"validity_days\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"plan_cost\",\"type\":\"double\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"activate_plan\",\"type\":\"enum\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Di0EWnavjnhMC3c5HkLOCt3UoWsxlnTHfwjk7pdU\"}','2017-08-13 17:20:35','2017-08-13 17:20:35'),(6,1,'admin/helpers/scaffold','GET','127.0.0.1','[]','2017-08-13 17:20:36','2017-08-13 17:20:36'),(7,1,'admin/auth/menu','GET','127.0.0.1','{\"_pjax\":\"#pjax-container\"}','2017-08-13 17:21:08','2017-08-13 17:21:08'),(8,1,'admin/auth/menu','POST','127.0.0.1','{\"parent_id\":\"0\",\"title\":\"SMS Plans\",\"icon\":\"fa-bars\",\"uri\":\"\\/sms-plan\",\"roles\":[\"1\",null],\"_token\":\"Di0EWnavjnhMC3c5HkLOCt3UoWsxlnTHfwjk7pdU\"}','2017-08-13 17:21:44','2017-08-13 17:21:44'),(9,1,'admin/auth/menu','GET','127.0.0.1','[]','2017-08-13 17:21:45','2017-08-13 17:21:45'),(10,1,'admin/auth/menu','GET','127.0.0.1','[]','2017-08-13 17:21:51','2017-08-13 17:21:51'),(11,1,'admin/sms-plan','GET','127.0.0.1','{\"_pjax\":\"#pjax-container\"}','2017-08-13 17:21:57','2017-08-13 17:21:57'),(12,1,'admin/sms-plan','GET','127.0.0.1','[]','2017-08-13 17:30:22','2017-08-13 17:30:22'),(13,1,'admin/sms-plan/create','GET','127.0.0.1','{\"_pjax\":\"#pjax-container\"}','2017-08-13 17:30:36','2017-08-13 17:30:36'),(14,1,'admin/sms-plan','POST','127.0.0.1','{\"plan_name\":\"Starter\",\"sms_quantity\":\"100\",\"validity_days\":\"7\",\"plan_cost\":\"1500\",\"activate_plan\":\"1\",\"_token\":\"Di0EWnavjnhMC3c5HkLOCt3UoWsxlnTHfwjk7pdU\",\"_previous_\":\"http:\\/\\/localhost:8000\\/admin\\/sms-plan\"}','2017-08-13 17:31:37','2017-08-13 17:31:37'),(15,1,'admin/sms-plan','GET','127.0.0.1','[]','2017-08-13 17:31:38','2017-08-13 17:31:38'),(16,1,'admin/sms-plan/create','GET','127.0.0.1','[]','2017-08-13 17:36:20','2017-08-13 17:36:20'),(17,1,'admin/sms-plan','POST','127.0.0.1','{\"plan_name\":\"Premium\",\"sms_quantity\":\"500\",\"validity_days\":\"10\",\"plan_cost\":\"6500\",\"activate_plan\":\"1\",\"_token\":\"YRZNhnBs8I9DqsUe66QVtb993YlNf1nAlDxjtHDn\"}','2017-08-13 17:40:40','2017-08-13 17:40:40'),(18,1,'admin/sms-plan','GET','127.0.0.1','[]','2017-08-13 17:40:40','2017-08-13 17:40:40'),(19,1,'admin/sms-plan/create','GET','127.0.0.1','{\"_pjax\":\"#pjax-container\"}','2017-08-13 17:40:44','2017-08-13 17:40:44'),(20,1,'admin/sms-plan','POST','127.0.0.1','{\"plan_name\":\"Business\",\"sms_quantity\":\"1000\",\"validity_days\":\"15\",\"plan_cost\":\"11000\",\"activate_plan\":\"1\",\"_token\":\"YRZNhnBs8I9DqsUe66QVtb993YlNf1nAlDxjtHDn\",\"_previous_\":\"http:\\/\\/localhost:8000\\/admin\\/sms-plan\"}','2017-08-13 17:41:24','2017-08-13 17:41:24'),(21,1,'admin/sms-plan','GET','127.0.0.1','[]','2017-08-13 17:41:24','2017-08-13 17:41:24'),(22,1,'admin/sms-plan/create','GET','127.0.0.1','{\"_pjax\":\"#pjax-container\"}','2017-08-13 17:41:27','2017-08-13 17:41:27'),(23,1,'admin/sms-plan','POST','127.0.0.1','{\"plan_name\":\"Gold\",\"sms_quantity\":\"5000\",\"validity_days\":\"25\",\"plan_cost\":\"50000\",\"activate_plan\":\"1\",\"_token\":\"YRZNhnBs8I9DqsUe66QVtb993YlNf1nAlDxjtHDn\",\"_previous_\":\"http:\\/\\/localhost:8000\\/admin\\/sms-plan\"}','2017-08-13 17:42:56','2017-08-13 17:42:56'),(24,1,'admin/sms-plan','GET','127.0.0.1','[]','2017-08-13 17:42:56','2017-08-13 17:42:56'),(25,1,'admin/sms-plan','GET','127.0.0.1','[]','2017-08-14 13:18:29','2017-08-14 13:18:29'),(26,1,'admin/auth/menu','GET','127.0.0.1','{\"_pjax\":\"#pjax-container\"}','2017-08-14 13:19:12','2017-08-14 13:19:12'),(27,1,'admin/auth/menu','POST','127.0.0.1','{\"parent_id\":\"0\",\"title\":\"All users\",\"icon\":\"fa-users\",\"uri\":\"\\/customers\",\"roles\":[\"1\",null],\"_token\":\"pSviy0jksikBg2ApftdCbGGoxXz4tW42oApnjqQj\"}','2017-08-14 13:19:53','2017-08-14 13:19:53'),(28,1,'admin/auth/menu','GET','127.0.0.1','[]','2017-08-14 13:19:54','2017-08-14 13:19:54'),(29,1,'admin/auth/menu','GET','127.0.0.1','[]','2017-08-14 13:19:59','2017-08-14 13:19:59');
/*!40000 ALTER TABLE `admin_operation_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_permissions`
--

DROP TABLE IF EXISTS `admin_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_permissions_name_unique` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_permissions`
--

LOCK TABLES `admin_permissions` WRITE;
/*!40000 ALTER TABLE `admin_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_role_menu`
--

DROP TABLE IF EXISTS `admin_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_role_menu`
--

LOCK TABLES `admin_role_menu` WRITE;
/*!40000 ALTER TABLE `admin_role_menu` DISABLE KEYS */;
INSERT INTO `admin_role_menu` VALUES (1,2,NULL,NULL),(1,8,NULL,NULL),(1,12,NULL,NULL),(1,13,NULL,NULL);
/*!40000 ALTER TABLE `admin_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_role_permissions`
--

DROP TABLE IF EXISTS `admin_role_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_role_permissions`
--

LOCK TABLES `admin_role_permissions` WRITE;
/*!40000 ALTER TABLE `admin_role_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_role_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_role_users`
--

DROP TABLE IF EXISTS `admin_role_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_role_users`
--

LOCK TABLES `admin_role_users` WRITE;
/*!40000 ALTER TABLE `admin_role_users` DISABLE KEYS */;
INSERT INTO `admin_role_users` VALUES (1,1,NULL,NULL);
/*!40000 ALTER TABLE `admin_role_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_roles`
--

DROP TABLE IF EXISTS `admin_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_roles_name_unique` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_roles`
--

LOCK TABLES `admin_roles` WRITE;
/*!40000 ALTER TABLE `admin_roles` DISABLE KEYS */;
INSERT INTO `admin_roles` VALUES (1,'Administrator','administrator','2017-08-13 17:07:17','2017-08-13 17:07:17');
/*!40000 ALTER TABLE `admin_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_user_permissions`
--

DROP TABLE IF EXISTS `admin_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_user_permissions`
--

LOCK TABLES `admin_user_permissions` WRITE;
/*!40000 ALTER TABLE `admin_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_users_username_unique` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_users`
--

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` VALUES (1,'admin','$2y$10$TINwxDe/UBK1kQJgIQA1meNjMh9AhByFyERh0Cl0RgfwSSmuBSz5S','Administrator',NULL,NULL,'2017-08-13 17:07:17','2017-08-13 17:07:17');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_smsplan`
--

DROP TABLE IF EXISTS `customer_smsplan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_smsplan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL COMMENT 'The ID of the customer',
  `smsplan_id` int(11) NOT NULL COMMENT 'The ID of the SMS Plan',
  `valid_till` datetime NOT NULL COMMENT 'The date of validity',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_smsplan`
--

LOCK TABLES `customer_smsplan` WRITE;
/*!40000 ALTER TABLE `customer_smsplan` DISABLE KEYS */;
INSERT INTO `customer_smsplan` VALUES (1,1,3,'2017-08-28 19:43:43','2017-08-13 17:43:43','2017-08-13 17:43:43');
/*!40000 ALTER TABLE `customer_smsplan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2017_07_15_154659_create_customer_smsplan_table',2),(4,'2017_07_15_155143_create_sms_plan_tabled',3),(5,'2016_01_04_173148_create_admin_tables',4),(6,'2017_07_15_155143_create_sms_plan_table',5);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `record_sending`
--

DROP TABLE IF EXISTS `record_sending`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `record_sending` (
  `id` bigint(100) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(100) NOT NULL,
  `text_sent` text NOT NULL,
  `receivers` text NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `record_sending`
--

LOCK TABLES `record_sending` WRITE;
/*!40000 ALTER TABLE `record_sending` DISABLE KEYS */;
INSERT INTO `record_sending` VALUES (1,1,'Hi all!\r\nThis message is to confirm that this is a working platform\nFree SMS on https://goo.gl/1PZE8Q','677315145 694034716','2017-08-16 00:38:32'),(2,1,'Hello All!\r\nThere is sandwich here for all\nFree SMS on https://goo.gl/1PZE8Q','677315145;694034716','2017-08-16 00:46:48'),(3,1,'Hello les gars\nFree SMS on https://goo.gl/1PZE8Q','677315145','2017-08-16 00:59:35'),(4,1,'Bonnour Kwaye. SVP garde le video projecteur demain soir pour notre formation\nFree SMS on https://goo.gl/1PZE8Q','677315145','2017-08-16 01:01:15');
/*!40000 ALTER TABLE `record_sending` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_plan`
--

DROP TABLE IF EXISTS `sms_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_plan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plan_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sms_quantity` int(11) NOT NULL,
  `validity_days` int(11) NOT NULL,
  `plan_cost` int(11) NOT NULL,
  `activate_plan` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sms_plan_plan_name_unique` (`plan_name`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_plan`
--

LOCK TABLES `sms_plan` WRITE;
/*!40000 ALTER TABLE `sms_plan` DISABLE KEYS */;
INSERT INTO `sms_plan` VALUES (1,'Starter',100,7,1500,1,'2017-08-13 17:31:38','2017-08-13 17:31:38'),(2,'Premium',500,10,6500,1,'2017-08-13 17:40:40','2017-08-13 17:40:40'),(3,'Business',1000,15,11000,1,'2017-08-13 17:41:24','2017-08-13 17:41:24'),(4,'Gold',5000,25,50000,1,'2017-08-13 17:42:56','2017-08-13 17:42:56');
/*!40000 ALTER TABLE `sms_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_validation` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_sender_name_unique` (`sender_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Ets K Soft Solutions','g.kwaye@ksoft-solutions.com','$2y$10$9vyFfkZs3ZKgW8xGkBKMhOsPY1k1prBwg2hSwfd3fEDLh1AyNagyq','K Soft','Kwaye Gabriel Kant','677315145',0,'n7ee1z2cdlLx0QwPA2TocNNYg3e4r7c8PLllgTguhchnn87EcsAaEiMuzLlq','2017-08-13 16:54:22','2017-08-13 16:54:22');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-16  1:04:50
