<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@subscription_handler')->name('home');

//Redirecto confirmation subscription
Route::get('/welcome', 'HomeController@subscription_handler')->name('welcome');

// Pack SMS
Route::get('/buy-sms-plan/confirm', 'Customer\CustomerBuyPlanController@confirmSMSPlan')->name('confirmSMSPlan');
Route::get('/buy-sms-plan', 'Customer\CustomerBuyPlanController@buy_sms_plan')->name('smsPlan');

Route::post('/buy-sms-plan', 'Customer\CustomerBuyPlanController@buy_sms_plan')->name('smsPlan');

Route::get('/sms-bulk-sms', 'Customer\CustomerSendSmsController@methodSendSMS')->name('formSMSToSend');
Route::post('/sms-bulk-sms', 'Customer\CustomerSendSmsController@methodSendSMS')->name('formSMSToSend');

Route::post('/sms-bulk-sent', 'Customer\CustomerSendSmsController@push')->name('push');
Route::get('/sms-bulk-sent/{message}/{to}/{from}', 'Customer\CustomerSendSmsController@push')->name('push');

Route::get('/billing/home', 'Customer\InvoiceController@index')->name('invoices');

Route::get('/report/list', 'Customer\CustomerSendSmsController@getLocalReport')->name('report');
