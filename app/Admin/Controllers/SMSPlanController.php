<?php

namespace App\Admin\Controllers;

use App\Models\SMSPlan;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class SMSPlanController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(SMSPlan::class, function (Grid $grid) {
            
            $grid->plan_name(trans('bulk.plan_name'));
            $grid->sms_quantity(trans('bulk.sms_quantity'));
            $grid->validity_days(trans('bulk.validity_days'));
            $grid->plan_cost(trans('bulk.plan_cost'));
            $grid->activate_plan(trans('bulk.status'));
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(SMSPlan::class, function (Form $form) {

            $form->display('id', 'ID');
            
            $form->text('plan_name', trans('bulk.plan_name'))->rules('required');
            $form->number('sms_quantity', trans('bulk.sms_quantity'))->rules('required');
            $form->number('validity_days', trans('bulk.validity_days'))->rules('required');
            $form->number('plan_cost', trans('bulk.plan_cost'))->rules('required');
            $form->radio('activate_plan', trans('bulk.activate_plan'))->options([1 => 'Activate', 0=> 'Inactive'])->default(0);

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
