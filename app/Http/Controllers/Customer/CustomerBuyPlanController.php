<?php
namespace App\Http\Controllers\Customer;

use App\Models\CustomerSMSPlan;
use App\Models\SMSPlan;
use Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CustomerBuyPlanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function buy_sms_plan(Request $request)
    {
        if($request->isMethod('POST')){
            $selected = $request->input('plan_name');
            $validity_days = $request->input("validity_days_$selected");
            if($selected == null){
                return redirect()->action('Customer\CustomerBuyPlanController@methodBuySmsPlan')->with('status', trans('bulk.plan_name_empty'));
            }
            $customer_id = Auth::user()->id;
            $sms_plan = $request->input('plan_name');
            $valid_till = date('Y-m-d H:i:s', strtotime("+ $validity_days day", time()));
            $inserted = DB::insert('insert into customer_smsplan(customer_id, smsplan_id, valid_till, created_at, updated_at) values(?,?,?,?,?)',
                      [$customer_id, $sms_plan, $valid_till, date('Y-m-d H:i:s', time()), date('Y-m-d H:i:s', time())]);
            if($inserted){
                return redirect()->action('Customer\CustomerBuyPlanController@confirmSMSPlan')->with('confirmSMSPlan', trans('bulk.confirm_sms_plan'));
            }
        }
        $packs = DB::table(config('admin.database.sms_plan_table'))->get();
        $data['plan'] = $packs;
        $data['all_plans'] = DB::table(config('admin.database.sms_plan_table'))->get();
        $data['last_plan'] = DB::table(config('admin.database.sms_plan_table'))->orderby('id', 'ASC')->limit(1)->get();
        return view('customer.buy_plan_sms', $data);
    }
    
    public function confirmSMSPlan(){
        //get all the registered plans for this customer
        $user = Auth::user();
        $data['allPlan'] = DB::table('customer_smsplan as CS')
            ->join('sms_plan as SP', 'SP.id', '=', 'CS.smsplan_id')
            ->where('CS.customer_id', '=', $user->id)
            ->get();
        $data['all_plans'] = DB::table(config('admin.database.sms_plan_table'))->get();
        $data['last_plan'] = DB::table(config('admin.database.sms_plan_table'))->orderby('id', 'ASC')->limit(1)->get();
        return view('customer.confirm_pack_sms', $data);
    }
    
    public function getUserLastPlan(Request $request){
        //return view('bulk');
    }

    public function buysmsplan(){
        echo "Hello";
    }
}
