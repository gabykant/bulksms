<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
        $all_plans = DB::table(config('admin.database.sms_plan_table'))->get();
        $last_plan = DB::table(config('admin.database.sms_plan_table'))->orderby('id', 'ASC')->limit(1)->get();
        return view('customer.invoices', ['last_plan' => $last_plan , 'all_plans' => $all_plans]);
    }
}
