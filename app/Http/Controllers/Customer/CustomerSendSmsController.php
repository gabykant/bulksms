<?php
namespace App\Http\Controllers\Customer;

use App\Models\CustomerSMSPlan;
use App\Models\SMSPlan;
use Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CustomerSendSmsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function methodSendSMS(Request $request)
    {
        if($request->isMethod('POST')){
            $from = Auth::user()->sender_name;
            $message = $request->input('msg');
            //$message .= "\nFree SMS on https://goo.gl/1PZE8Q";
            //$message = urlencode($message);
            
            // Convert the incoming variable to Array
            $to = $request->input("send_to");
            $to = explode(";", $to);
            //$smsto = array();
            $smsto = "";
            if(!is_array($to)){exit;}
            $length = count($to);
            for($i=0; $i<$length; $i++){
                /*$nospace = preg_replace('/\s+/', '', $to[$i]);
                array_push($smsto, "tel:+237$nospace");*/
                if(($i+1) >= $length){
                    $smsto .= preg_replace('/\s+/', '', $to[$i]);
                }else{
                    $smsto .= preg_replace('/\s+/', '', $to[$i]) . " ";
                }
                $nospace = preg_replace('/\s+/', '', $to[$i]);
                $this->push($request, $message, $nospace, $from);
            }
            
            // Save to database all transaction before sending them to recipients
            $this->saveToDb(Auth::user()->id, $smsto, $message);
            //$this->push($message, $smsto, $from);
            // Write to session and display the sucessful result
            
        }
        
        $data['all_plans'] = DB::table(config('admin.database.sms_plan_table'))->get();
        $data['last_plan'] = DB::table(config('admin.database.sms_plan_table'))->orderby('id', 'ASC')->limit(1)->get();
        $data['number_sms_sent'] = count(DB::table(config('admin.database.sms_record'))->where(['user_id'=> Auth::user()->id])->get());
        return view('customer.send_sms_form', $data);
    }

    protected function push(Request $request, $message, $to, $from){
        $token = $this->getAccessToken();

        $remainingSMS = $this->getRemainingSMS($request, $token);
        if($remainingSMS <= 0) {
            $request->session()->flash('status:error', 'Your balance is insufficent to continue the operation. Please buy a new SMS Pack or renew your bundle');
        
        }else{
            $headers = array(
                'Authorization: Bearer ' . $token,
                'Content-Type: application/json'
            );
            $fields = array(
                "outboundSMSMessageRequest" => array(
                    "address" => "tel:+237$to",
                    "senderAddress" => "tel:+22500000000",
                    "outboundSMSTextMessage" => array(
                        "message" => $message
                    )
                )
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, config('sms.remote_server') . '/smsmessaging/v1/outbound/tel%3A%2B22500000000/requests');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $request->session()->flash('status:success', 'Your message has been delivered to your recipients');
        }
    }
    
    /*protected function push($message, $to, $from){
        // from
        $url = "http://52.35.166.25:13013/cgi-bin/sendsms?";
        $username="1universy";
        $password="RockIn20!6@";
        
        $request = http_build_query([
            'username' => $username,
            'password' => $password,
            'text' => urlencode($message),
            'to' => $to,
            'from' => urlencode($from)
        ]);
	    //$request = urldecode($request);
        $curl = curl_init();
        //curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    //curl_setopt($curl, CURLOPT_URL, $url2);
        curl_setopt($curl, CURLOPT_URL, $url.$request);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);

	    curl_close($curl);
    }*/
    
    private function saveToDb($sender_id, $receivers, $sent){
        $inserted_sms = DB::table('record_sending')->insert([
            "user_id" => $sender_id,
            "text_sent" => $sent,
            "receivers" => preg_replace('/\s+/',';', $receivers)
        ]);
    }

    private function getAccessToken(){
        $url = config('sms.remote_server') . "/oauth/v2/token";
        $credentials = config('sms.remote_client_ID') . ':' . config('sms.remote_client_secret');
        $headers = array('Authorization: Basic ' . base64_encode($credentials));
        $args = array('grant_type' => 'client_credentials');
        $fields = http_build_query([
            'grant_type' => 'client_credentials'
        ]);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $result = curl_exec($ch);
        if( $result === false){
            //echo curl_error($ch);
        }else{
            //var_dump($result);
        }
        //close connection
        curl_close($ch);

        $r = json_decode($result);
        return $r->access_token;
    }

    private function getRemainingSMS(Request $request, $token){
        $headers = array(
            'Authorization: Bearer ' . $token
        );
        $url = "https://api.orange.com/sms/admin/v1/contracts";
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        $output = curl_exec($ch);
        $r = json_decode($output);
        $o = $r->partnerContracts->contracts[0]->serviceContracts[0]->availableUnits;
        //$request = new \Illuminate\Http\Request;
        $request->session()->put('remainingSMS', $o);
        //echo $r['partnerContracts']['contracts'][0];
        /*echo curl_errno($ch);
        var_dump($output);*/
        curl_close($ch);
        return $o;
    }

    public function getLocalReport(){
        $user = Auth::user();
        $data['getSent'] = DB::table('record_sending')
            ->where('user_id', '=', $user->id)
            ->get();
        $data['last_plan'] = DB::table(config('admin.database.sms_plan_table'))->orderby('id', 'ASC')->limit(1)->get();
        return view('customer.report_sent_sms', $data);
    }
}
