<?php

namespace App\Http\Controllers\Customer;

use App\Models\CustomerSMSPlan;
use App\Models\SMSPlan;
use Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CustomerBuyPlanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function push_sms(Request $request){
        if($request->isMethod('post')){
            $sender_id = $request->input('from');
            $to = $request->input('to');
            $msg = $request->input('msg');
            $this->push($msg, $to, $from);
            $this->saveToDb($sender_id, $to, $msg);
            return json_encode();
        }
    }

    protected function push($message, $to, $from){
        // from
        $url = config('user.remote_server');
        $username = config('user.remote_username');
        $password = config('user.remote_password');
        
        $request = http_build_query([
            'username' => $username,
            'password' => $password,
            'text' => urlencode($message),
            'to' => $to,
            'from' => $from
        ]);

        $request = urldecode($request);

        $curl = curl_init();

        //curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($curl, CURLOPT_URL, $url);
        //curl_setopt($curl, CURLOPT_URL, $url.$request);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);

	    curl_close($curl);
        //print_r($curl . " Output data");
        //print_r($return);
    }

    private function check_login($sender_name){

    }

    private function saveToDb($sender_id, $receivers, $sent){
        $inserted_sms = DB::table('record_sending')->insert([
            "user_id" => $sender_id,
            "text_sent" => $sent,
            "receivers" => preg_replace('/\s+/',';', $receivers)
        ]);
    }
}