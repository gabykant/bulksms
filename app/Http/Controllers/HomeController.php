<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Khill\Lavacharts\Lavacharts;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_plans = DB::table(config('admin.database.sms_plan_table'))->get();
        $last_plan = DB::table(config('admin.database.sms_plan_table'))->orderby('id', 'ASC')->limit(1)->get();

        $population = \Lava::DataTable();
        
        $population->addDateColumn('Year')
                   ->addNumberColumn('Number of People')
                   ->addRow(['2006', 623452])
                   ->addRow(['2007', 685034])
                   ->addRow(['2008', 716845])
                   ->addRow(['2009', 757254])
                   ->addRow(['2010', 778034])
                   ->addRow(['2011', 792353])
                   ->addRow(['2012', 839657])
                   ->addRow(['2013', 842367])
                   ->addRow(['2014', 873490]);
        
        \Lava::AreaChart('Population', $population, [
            'title' => 'Population Growth',
            'legend' => [
                'position' => 'in'
            ]
        ]);

        return view('home', ['last_plan' => $last_plan , 'all_plans' => $all_plans]);
    }
    
    /**
     * Confirm the creation of the account
     *
     * @return void
     */
    public function subscription_handler()
    {
        $all_plans = DB::table(config('admin.database.sms_plan_table'))->get();
        $last_plan = DB::table(config('admin.database.sms_plan_table'))->orderby('id')->limit(1)->get();
        $number_sms_sent = DB::table(config('admin.database.sms_record'))->select(DB::raw('count(id) as total_sms'),'updated_date')
            ->where(['user_id'=> Auth::user()->id])
            ->groupby(DB::raw('DATE(updated_date)'))->get()->toArray();
        $allSMS = \Lava::DataTable();
        $allSMS->addDateColumn('Year')
                   ->addNumberColumn('Number of SMS')
                   ->setDateTimeFormat('Y');
        for($i = 0; $i < count($number_sms_sent); $i++){
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $number_sms_sent[$i]->updated_date);
            $allSMS->addRow([$date, $number_sms_sent[$i]->total_sms]);
        }
        
        \Lava::ColumnChart('allSMS', $allSMS, [
            'title' => 'All SMS sent successful',
            'legend' => [
                'position' => 'in'
            ]
        ]);

        return view('customer.subscription_handler', ['last_plan' => $last_plan , 'all_plans' => $all_plans, 'remaining'=> count($number_sms_sent)]);
    }
}
