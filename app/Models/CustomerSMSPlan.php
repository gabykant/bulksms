<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerSMSPlan extends Model
{
    protected $table = 'customer_smsplan';
}
