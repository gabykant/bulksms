-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2017 at 01:50 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bulksms`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'Index', 'fa-bar-chart', '/', NULL, NULL),
(2, 0, 2, 'Admin', 'fa-tasks', '', NULL, NULL),
(3, 2, 3, 'Users', 'fa-users', 'auth/users', NULL, NULL),
(4, 2, 4, 'Roles', 'fa-user', 'auth/roles', NULL, NULL),
(5, 2, 5, 'Permission', 'fa-user', 'auth/permissions', NULL, NULL),
(6, 2, 6, 'Menu', 'fa-bars', 'auth/menu', NULL, NULL),
(7, 2, 7, 'Operation log', 'fa-history', 'auth/logs', NULL, NULL),
(8, 0, 8, 'Helpers', 'fa-gears', '', NULL, NULL),
(9, 8, 9, 'Scaffold', 'fa-keyboard-o', 'helpers/scaffold', NULL, NULL),
(10, 8, 10, 'Database terminal', 'fa-database', 'helpers/terminal/database', NULL, NULL),
(11, 8, 11, 'Laravel artisan', 'fa-terminal', 'helpers/terminal/artisan', NULL, NULL),
(12, 0, 0, 'SMS Plans', 'fa-bars', '/sms-plan', '2017-08-13 17:21:44', '2017-08-13 17:21:44'),
(13, 0, 0, 'All users', 'fa-users', '/customers', '2017-08-14 13:19:53', '2017-08-14 13:19:53');

-- --------------------------------------------------------

--
-- Table structure for table `admin_operation_log`
--

CREATE TABLE `admin_operation_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_operation_log`
--

INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'GET', '127.0.0.1', '[]', '2017-08-13 17:09:25', '2017-08-13 17:09:25'),
(2, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-08-13 17:13:32', '2017-08-13 17:13:32'),
(3, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{"table_name":"sms_plan","model_name":"App\\\\Models\\\\SMSPlan","controller_name":"App\\\\Admin\\\\Controllers\\\\SMSPlanController2","create":["migration","model","controller","migrate"],"fields":[{"name":"plan_name","type":"string","key":null,"default":null,"comment":null},{"name":"sms_quantity","type":"integer","key":null,"default":null,"comment":null},{"name":"validity_days","type":"integer","key":null,"default":null,"comment":null},{"name":"plan_cost","type":"double","key":null,"default":null,"comment":null},{"name":"activate_plan","type":"enum","key":null,"default":null,"comment":null}],"timestamps":"on","primary_key":"id","_token":"Di0EWnavjnhMC3c5HkLOCt3UoWsxlnTHfwjk7pdU"}', '2017-08-13 17:19:31', '2017-08-13 17:19:31'),
(4, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-08-13 17:19:32', '2017-08-13 17:19:32'),
(5, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{"table_name":"sms_plan","model_name":"App\\\\Models\\\\SMSPlan","controller_name":"App\\\\Admin\\\\Controllers\\\\SMSPlanController2","create":["model","controller","migrate"],"fields":[{"name":"plan_name","type":"string","key":null,"default":null,"comment":null},{"name":"sms_quantity","type":"integer","key":null,"default":null,"comment":null},{"name":"validity_days","type":"integer","key":null,"default":null,"comment":null},{"name":"plan_cost","type":"double","key":null,"default":null,"comment":null},{"name":"activate_plan","type":"enum","key":null,"default":null,"comment":null}],"timestamps":"on","primary_key":"id","_token":"Di0EWnavjnhMC3c5HkLOCt3UoWsxlnTHfwjk7pdU"}', '2017-08-13 17:20:35', '2017-08-13 17:20:35'),
(6, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-08-13 17:20:36', '2017-08-13 17:20:36'),
(7, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2017-08-13 17:21:08', '2017-08-13 17:21:08'),
(8, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"0","title":"SMS Plans","icon":"fa-bars","uri":"\\/sms-plan","roles":["1",null],"_token":"Di0EWnavjnhMC3c5HkLOCt3UoWsxlnTHfwjk7pdU"}', '2017-08-13 17:21:44', '2017-08-13 17:21:44'),
(9, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2017-08-13 17:21:45', '2017-08-13 17:21:45'),
(10, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2017-08-13 17:21:51', '2017-08-13 17:21:51'),
(11, 1, 'admin/sms-plan', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2017-08-13 17:21:57', '2017-08-13 17:21:57'),
(12, 1, 'admin/sms-plan', 'GET', '127.0.0.1', '[]', '2017-08-13 17:30:22', '2017-08-13 17:30:22'),
(13, 1, 'admin/sms-plan/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2017-08-13 17:30:36', '2017-08-13 17:30:36'),
(14, 1, 'admin/sms-plan', 'POST', '127.0.0.1', '{"plan_name":"Starter","sms_quantity":"100","validity_days":"7","plan_cost":"1500","activate_plan":"1","_token":"Di0EWnavjnhMC3c5HkLOCt3UoWsxlnTHfwjk7pdU","_previous_":"http:\\/\\/localhost:8000\\/admin\\/sms-plan"}', '2017-08-13 17:31:37', '2017-08-13 17:31:37'),
(15, 1, 'admin/sms-plan', 'GET', '127.0.0.1', '[]', '2017-08-13 17:31:38', '2017-08-13 17:31:38'),
(16, 1, 'admin/sms-plan/create', 'GET', '127.0.0.1', '[]', '2017-08-13 17:36:20', '2017-08-13 17:36:20'),
(17, 1, 'admin/sms-plan', 'POST', '127.0.0.1', '{"plan_name":"Premium","sms_quantity":"500","validity_days":"10","plan_cost":"6500","activate_plan":"1","_token":"YRZNhnBs8I9DqsUe66QVtb993YlNf1nAlDxjtHDn"}', '2017-08-13 17:40:40', '2017-08-13 17:40:40'),
(18, 1, 'admin/sms-plan', 'GET', '127.0.0.1', '[]', '2017-08-13 17:40:40', '2017-08-13 17:40:40'),
(19, 1, 'admin/sms-plan/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2017-08-13 17:40:44', '2017-08-13 17:40:44'),
(20, 1, 'admin/sms-plan', 'POST', '127.0.0.1', '{"plan_name":"Business","sms_quantity":"1000","validity_days":"15","plan_cost":"11000","activate_plan":"1","_token":"YRZNhnBs8I9DqsUe66QVtb993YlNf1nAlDxjtHDn","_previous_":"http:\\/\\/localhost:8000\\/admin\\/sms-plan"}', '2017-08-13 17:41:24', '2017-08-13 17:41:24'),
(21, 1, 'admin/sms-plan', 'GET', '127.0.0.1', '[]', '2017-08-13 17:41:24', '2017-08-13 17:41:24'),
(22, 1, 'admin/sms-plan/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2017-08-13 17:41:27', '2017-08-13 17:41:27'),
(23, 1, 'admin/sms-plan', 'POST', '127.0.0.1', '{"plan_name":"Gold","sms_quantity":"5000","validity_days":"25","plan_cost":"50000","activate_plan":"1","_token":"YRZNhnBs8I9DqsUe66QVtb993YlNf1nAlDxjtHDn","_previous_":"http:\\/\\/localhost:8000\\/admin\\/sms-plan"}', '2017-08-13 17:42:56', '2017-08-13 17:42:56'),
(24, 1, 'admin/sms-plan', 'GET', '127.0.0.1', '[]', '2017-08-13 17:42:56', '2017-08-13 17:42:56'),
(25, 1, 'admin/sms-plan', 'GET', '127.0.0.1', '[]', '2017-08-14 13:18:29', '2017-08-14 13:18:29'),
(26, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2017-08-14 13:19:12', '2017-08-14 13:19:12'),
(27, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{"parent_id":"0","title":"All users","icon":"fa-users","uri":"\\/customers","roles":["1",null],"_token":"pSviy0jksikBg2ApftdCbGGoxXz4tW42oApnjqQj"}', '2017-08-14 13:19:53', '2017-08-14 13:19:53'),
(28, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2017-08-14 13:19:54', '2017-08-14 13:19:54'),
(29, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2017-08-14 13:19:59', '2017-08-14 13:19:59');

-- --------------------------------------------------------

--
-- Table structure for table `admin_permissions`
--

CREATE TABLE `admin_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_roles`
--

CREATE TABLE `admin_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_roles`
--

INSERT INTO `admin_roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'administrator', '2017-08-13 17:07:17', '2017-08-13 17:07:17');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_menu`
--

CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_menu`
--

INSERT INTO `admin_role_menu` (`role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL),
(1, 8, NULL, NULL),
(1, 12, NULL, NULL),
(1, 13, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_permissions`
--

CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_users`
--

CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_users`
--

INSERT INTO `admin_role_users` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `username`, `password`, `name`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$TINwxDe/UBK1kQJgIQA1meNjMh9AhByFyERh0Cl0RgfwSSmuBSz5S', 'Administrator', NULL, NULL, '2017-08-13 17:07:17', '2017-08-13 17:07:17');

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_permissions`
--

CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_smsplan`
--

CREATE TABLE `customer_smsplan` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL COMMENT 'The ID of the customer',
  `smsplan_id` int(11) NOT NULL COMMENT 'The ID of the SMS Plan',
  `valid_till` datetime NOT NULL COMMENT 'The date of validity',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_smsplan`
--

INSERT INTO `customer_smsplan` (`id`, `customer_id`, `smsplan_id`, `valid_till`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2017-08-28 19:43:43', '2017-08-13 17:43:43', '2017-08-13 17:43:43');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_07_15_154659_create_customer_smsplan_table', 2),
(4, '2017_07_15_155143_create_sms_plan_tabled', 3),
(5, '2016_01_04_173148_create_admin_tables', 4),
(6, '2017_07_15_155143_create_sms_plan_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sms_plan`
--

CREATE TABLE `sms_plan` (
  `id` int(10) UNSIGNED NOT NULL,
  `plan_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sms_quantity` int(11) NOT NULL,
  `validity_days` int(11) NOT NULL,
  `plan_cost` int(11) NOT NULL,
  `activate_plan` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sms_plan`
--

INSERT INTO `sms_plan` (`id`, `plan_name`, `sms_quantity`, `validity_days`, `plan_cost`, `activate_plan`, `created_at`, `updated_at`) VALUES
(1, 'Starter', 100, 7, 1500, 1, '2017-08-13 17:31:38', '2017-08-13 17:31:38'),
(2, 'Premium', 500, 10, 6500, 1, '2017-08-13 17:40:40', '2017-08-13 17:40:40'),
(3, 'Business', 1000, 15, 11000, 1, '2017-08-13 17:41:24', '2017-08-13 17:41:24'),
(4, 'Gold', 5000, 25, 50000, 1, '2017-08-13 17:42:56', '2017-08-13 17:42:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_validation` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `sender_name`, `full_name`, `phone_number`, `account_validation`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ets K Soft Solutions', 'g.kwaye@ksoft-solutions.com', '$2y$10$9vyFfkZs3ZKgW8xGkBKMhOsPY1k1prBwg2hSwfd3fEDLh1AyNagyq', 'K Soft', 'Kwaye Gabriel Kant', '677315145', 0, 'n7ee1z2cdlLx0QwPA2TocNNYg3e4r7c8PLllgTguhchnn87EcsAaEiMuzLlq', '2017-08-13 16:54:22', '2017-08-13 16:54:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_operation_log_user_id_index` (`user_id`);

--
-- Indexes for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_permissions_name_unique` (`name`);

--
-- Indexes for table `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_roles_name_unique` (`name`);

--
-- Indexes for table `admin_role_menu`
--
ALTER TABLE `admin_role_menu`
  ADD KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`);

--
-- Indexes for table `admin_role_permissions`
--
ALTER TABLE `admin_role_permissions`
  ADD KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`);

--
-- Indexes for table `admin_role_users`
--
ALTER TABLE `admin_role_users`
  ADD KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_users_username_unique` (`username`);

--
-- Indexes for table `admin_user_permissions`
--
ALTER TABLE `admin_user_permissions`
  ADD KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`);

--
-- Indexes for table `customer_smsplan`
--
ALTER TABLE `customer_smsplan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sms_plan`
--
ALTER TABLE `sms_plan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sms_plan_plan_name_unique` (`plan_name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_sender_name_unique` (`sender_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin_roles`
--
ALTER TABLE `admin_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `customer_smsplan`
--
ALTER TABLE `customer_smsplan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sms_plan`
--
ALTER TABLE `sms_plan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
