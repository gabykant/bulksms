<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Bulk SMS Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during Bulk SMS Content for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'plan_name' => 'Bulk SMS Plan',
    'sms_quantity' => 'SMS Quantity',
    'validity_days' => 'Validity days',
    'plan_cost' => 'Plan Cost',
    'activate_plan' => 'Is the plan activate ?',
    'status' => 'Status',
    'plan_name_empty' => 'Please choose a plan to subscribe to',
    'confirm_sms_plan'  => 'Hello! You\re order has been confirmed',

];
