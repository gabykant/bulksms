<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Bulk SMS Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during Bulk SMS Content for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'plan_name' => 'Pack SMS',
    'sms_quantity' => 'Quantité',
    'validity_days' => 'Validité',
    'plan_cost' => 'Coût',
    'activate_plan' => 'Activé ?',
    'status' => 'Statut',

];