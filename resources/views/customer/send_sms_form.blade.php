@extends('layouts.template')

@section('content')

        <div class="col-md-9">
            @if(Session::has('status:success'))
                <p class="alert alert-success">
                    {{ Session::get('status:success') }}
                </p>
            @endif

            @if(Session::has('status:error'))
                <p class="alert alert-danger">
                    {{ Session::get('status:error') }}
                </p>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">Send Bulk SMS</div>
                <div class="panel-body extend-panel-body">
                     <form class="form-horizontal" method="POST" action="{{ route('formSMSToSend') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('sender_name') ? ' has-error' : '' }}">
                                        <label for="sender_name" class="col-md-4 control-label">From <em>(Sender ID)</em></label>
                                        <div class="col-md-6">
                                            <input id="sender_name" type="text" class="form-control" name="sender_name" value="{{ Auth::user()->sender_name }}" required autofocus disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('send_to') ? ' has-error' : '' }}">
                                        <label for="send_to" class="col-md-4 control-label">To </label>
                                        <div class="col-md-6">
                                            <textarea rows="10" cols="5" id="send_to" class="form-control" name="send_to" value="{{ old('send_to') }}" required autofocus ></textarea>
                                            <em>Separate each number with a <strong>semi-comma ;</strong></em>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('msg') ? ' has-error' : '' }}">
                                        <label for="msg" class="col-md-4 control-label">Message </label>
                                        <div class="col-md-6">
                                            <textarea rows="10" cols="5" id="message_to_send" class="form-control" name="msg" value="{{ old('msg') }}" required autofocus></textarea>
                                            <em>You are limited to 140 characters</em>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('sending_schedule') ? ' has-error' : '' }}">
                                        <label for="sending_schedule" class="col-md-4 control-label"></label>
                                        <div class="col-md-6">
                                            <!--<a href="">Schedule and send later ? Set date and time here</a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                         <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                @if($number_sms_sent < 600)
                                    <button type="submit" class="btn btn-primary">
                                        Send Now!
                                    </button>
                                @else
                                    <p class="alert alert-danger">Sorry you exceed the number of the allowed free SMS</p>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection