@extends('layouts.template')

@section('content')

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Report sent SMS</div>
                <div class="panel-body extend-panel-body">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th>Date operation</th>
                                <th>Receivers</th>
                                <th>Message</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($getSent) > 0)
                                @foreach($getSent as $s)
                                    <tr>
                                        <td width="20%">{{ $s->updated_date }}</td>
                                        <td width="60%">{{ $s->receivers }}</td>
                                        <td width="20%">{{ str_limit($s->text_sent, 10, '..') }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="">No data was found</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection