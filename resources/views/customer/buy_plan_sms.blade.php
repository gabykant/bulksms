@extends('layouts.template')

@section('content')
<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading">Buy a SMS Pack</div>
        <div class="panel-body">
            <form class="form-horizontal" method="POST" action="{{ route('buySmsPlan') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">

                            @if (session('status'))
                                <div class="alert alert-warning">
                                    {{ session('status') }}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-md-offset-1">
                            <ul style="list-style: none; margin:0; padding:0">
                                @foreach($plan as $key=>$value)
                                    <li>
                                        <label style="display: block;">
                                            <input type="radio" name="plan_name" value={{ $value->id }} />&nbsp;&nbsp;&nbsp;&nbsp;{{ $value->plan_name }}
                                        </label>

                                        <ul>
                                            <li>Quantity : {{ $value->sms_quantity }} SMS</li>
                                            <li>Validity : {{ $value->validity_days }} Days</li>
                                            <input type="hidden" name="validity_days_{{ $value->id }}" value={{ $value->validity_days }} />
                                            <li>Cost : {{ $value->plan_cost }}</li>
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-1">
                        <button type="submit" class="btn btn-primary">
                            Buy Now
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>
@endsection