@extends('layouts.template')

@section('content')
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-body extend-panel-body">
                <div id="pop_div"></div>
                <?= Lava::render('ColumnChart', 'allSMS', 'pop_div') ?>
            </div>
        </div>
    </div>
</div>
</div>
@endsection