@extends('layouts.template')

@section('content')

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Your Pack SMS</div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <th>Subscription plan</th>
                            <th>Quantity (SMS)</th>
                            <th>Validity (Days)</th>
                            <th>Cost (Frs)</th>
                            <th>Started At</th>
                            <th>End At</th>
                        </thead>
                        <tbody>
                            @foreach($allPlan as $key=>$value)
                                <tr>
                                    <td>{{ $value->plan_name }}</td>
                                    <td>{{ $value->sms_quantity }}</td>
                                    <td>{{ $value->validity_days }}</td>
                                    <td>{{ $value->plan_cost }}</td>
                                    <td>{{ $value->created_at }}</td>
                                    <td>{{ $value->valid_till }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection