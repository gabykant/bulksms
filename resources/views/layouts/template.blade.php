<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>Mass communication with Bulk SMS</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="SMS, Bulk, BULK, Bulk SMS, SMS Bulk, Restful API for bulk SMS, API, Restful API SMS" />

	<meta name="description" content="A platform to send bulk SMS in a secure and reliable way"/>
	<meta name="subject" content="A Bulk SMS Platform">
	<meta name="copyright"content="K Soft Solutions">
	<meta name="language" content="en_EN">
	<meta name="robots" content="index,follow" />
	<meta name="abstract" content="">
	<meta name="topic" content="">
	<meta name="summary" content="">
	<meta name="Classification" content="News">
	<meta name="author" content="Gabriel Kwaye, gabrielkwaye@gmail.com">
	<meta name="author" content="Bulk SMS" >
	<meta name="designer" content="Gabriel Kwaye, gabrielkwaye@gmail.com">
	<meta name="copyright" content="2017, K Soft Solutions">
	<meta name="reply-to" content="contact@hosting-cm.com">
	<meta name="owner" content="">
	<meta name="url" content="http://sms.hosting-cm.com">
	<meta name="identifier-URL" content="http://sms.hosting-cm.com">
	<meta name="directory" content="submission">
	<meta name="category" content="business">
	<meta name="coverage" content="Worldwide">
	<meta name="distribution" content="Global">
	<meta name="rating" content="General">
	<meta name="revisit-after" content="7 days">
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache">
		
	<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet"> 
	<link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
	<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href="{{ asset('css/immersive-slider.css') }}" rel='stylesheet' type='text/css'>
</head>
<body>
<style>
ul#top_menu > li > a {
    text-decoration: none;
}
.dashboard{
    color: #fff;
    text-decoration: none;
    background-color: #0101DF !important;
    outline: 0;
}
</style>
    <div class="header-top">
		<div class="container">
			<div class="w3layouts-address">
				<ul>
					<li><i class="fa fa-mobile" aria-hidden="true"></i> +237 677 315 145</li>
					<li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:info@example.com"> info@sms.hosting-cm.com</a></li>
				</ul>
			</div>
			<div class="agileinfo-social-grids">
				<ul id="top_menu">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a>&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                    @if (Auth::check())
                        <li><span style="color: #ccc">Welcome, {{ Auth::user()->full_name}}</span></li>
                    @else
                        <li><a href="{{ url('/login') }}" style="background-color: #e6e6e6; border-radius: 2px; border:1px solid #e6e6e6; padding: 5px 10px;"><i class="fa fa-sign-in"></i>&nbsp;Login</a></li>
                        <!-- <li>&nbsp;<a href="{{ url('/register') }}" style="background-color: #e6e6e6; border-radius: 2px; border:1px solid #e6e6e6; padding: 5px 10px;">Register&nbsp;<i class="fa fa-user-plus"></i></a></li> -->
                    @endif
				</ul>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>

    <div class="header">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <div class="w3layouts-logo">
                        <h1><a href="{{ url('/') }}">Bulk SMS <span>Mass communication</span></a></h1>
                    </div>
                </div>
                <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                    <nav>
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="{{ url('/') }}">Home</a></li>
                            <li><a href="plans.html" class="hvr-sweep-to-bottom">SMS Packs</a></li>
                            <li><a href="blog.html" class="hvr-sweep-to-bottom">Big Data</a></li>
                            <li><a href="mail.html" class="hvr-sweep-to-bottom">Request quotes</a></li>
                            @if (Auth::check())
                                <li class="dashboard"><a href="{{ url('/welcome') }}">Send SMS</li>
                            @endif
                        </ul>
                    </nav>
                </div>
            </nav>
        </div>
	</div>
    @if (Auth::check())
    <div class="main">
        <div class="container">
            <div class="col-md-3">
                <div class="sidebars">
                    <div panel panel-default panel-flush>
                        <ul>
                            <!-- <li>
                                <a href="{{ url('/buy-sms-plan') }}">
                                    Buy a packss
                                </a>
                            </li> -->
                            <li>
                                <a href="{{ route('formSMSToSend') }}">
                                    Send SMS
                                </a>
                            </li>
                            <!-- <li>
                                <a href="{{ route('confirmSMSPlan') }}">
                                    My SMS Plan
                                </a>
                            </li> -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->full_name }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>    
                    </div>   
                    <div class="panel panel-default panel-flush">
                        <div class="panel-heading">
                            Remaining : @if(Session::has('remainingSMS')) 
                            <b>{{ Session::get('remainingSMS') }}</b> SMS
                            @endif
                        </div>
                    </div> 
                    <div class="panel panel-default panel-flush">
                        <div class="panel-heading">
                            <a href="{{ route('report') }}">Report list</a>
                        </div>
                    </div>
                    <!-- <div class="panel panel-default panel-flush">
                        <div class="panel-heading">
                            Subscribed plan
                        </div>
                        <div class="panel-body">
                            <ul class="nav">
                                @foreach($last_plan as $p)
                                <li role="presentation"><a href=""><i class="fa fa-caret-right"></i>{{$p->plan_name}}</a></li>
                                    <strong>Remaining : {{ $p->sms_quantity }}</strong>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="panel panel-default panel-flush">
                        <div class="panel-heading">
                            Billing
                        </div>
                        <div class="panel-body">
                            <ul class="nav">
                                <li role="presentation"><a href=""><i class="fa fa-dollar"></i>&nbsp;&nbsp;Payment methods</a></li>
                                <li role="presentation"><a href="{{ route('invoices') }}"><i class="fa fa-th-list"></i>&nbsp;&nbsp;Invoices</a></li>
                            </ul>
                        </div>
                    </div> -->

                </div>
            </div>
    @endif
    @yield('content')
    <div class="footer">
		<div class="container">
			<div class="agile-footer-grids">
				<div class="col-md-3 agile-footer-grid">
					<h4>Our Services</h4>
					<ul class="w3agile_footer_grid_list">
						<li><a href="#">Consulting</a> </li>
						<li><a href="#">Training</a></li>
					</ul>
				</div>
				<div class="col-md-3 agile-footer-grid">
					<h4>Our Products</h4>
					<ul class="w3agile_footer_grid_list">
						<li><a href="https://www.hosting-cm.com">Web Hosting</a></li>
						<li><a href="https://www.ksoft-solutions.com">Application</a></li>
					</ul>
				</div>
				<div class="col-md-3 agile-footer-grid">
					<h4>Our Company</h4>
					<ul class="w3agile_footer_grid_list">
						<li> <a href="#">About your Company</a></li>
						<li> <a href="#">Terms & conditions</a></li>
						<li> <a href="#">Testimonials</a></li>
					</ul>
				</div>
				<div class="col-md-3 agile-footer-grid">
					<h4>Follow Us</h4>
					<ul class="w3agile_footer_grid_list">
						<li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i>	JOIN US ON FACEBOOK</a> 
						</li>
						<li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i>	Follow us on Twitter</a></li>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="copyright">
			<p>© 2017 K Soft Solutions. All rights reserved </p>
		</div>
	</div>

</body>
</html>
<script type="application/x-javascript"> 
    addEventListener("load", function() { 
            setTimeout(hideURLbar, 0); 
    }, false);
    function hideURLbar(){ 
        window.scrollTo(0,1); 
    } 
</script>
<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.js') }}"></script>
<script src="{{ asset('js/SmoothScroll.min.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){		
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<script type="text/javascript">
    $(document).ready( function() {
        $("#immersive_slider").immersive_slider({
                container: ".main"
        });
    });
</script>
<script src="{{ asset('js/jarallax.js') }}"></script>
<script type="text/javascript">
    /* init Jarallax */
    $('.jarallax').jarallax({
        speed: 0.5,
        imgWidth: 1366,
        imgHeight: 768
    })
</script>
<script src="{{ asset('js/responsiveslides.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/move-top.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/easing.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear' 
            };
        */
        $().UItoTop({ easingType: 'easeOutQuart' });
    });
</script>
<script type="text/javascript" src="{{ asset('js/jquery.immersive-slider.js') }}"></script>
