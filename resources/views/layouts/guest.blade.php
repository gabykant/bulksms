<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <meta name="keywords" content="SMS, Bulk, BULK, Bulk SMS, SMS Bulk, Restful API for bulk SMS, API, Restful API SMS"/>
    <meta name="description" content="A platform to send bulk SMS in a secure and reliable way"/>
    <meta name="subject" content="A Bulk SMS Platform">
    <meta name="copyright"content="K Soft Solutions">
    <meta name="language" content="en_EN">
    <meta name="robots" content="index,follow" />
    <meta name="abstract" content="">
    <meta name="topic" content="">
    <meta name="summary" content="">
    <meta name="Classification" content="News">
    <meta name="author" content="Gabriel Kwaye, gabrielkwaye@gmail.com">
    
    <meta name="author" content="Bulk SMS" >

    <meta name="designer" content="Gabriel Kwaye, gabrielkwaye@gmail.com">
    <meta name="copyright" content="2017, K Soft Solutions">
    <meta name="reply-to" content="contact@hosting-cm.com">
    <meta name="owner" content="">
    <meta name="url" content="http://sms.hosting-cm.com">
    <meta name="identifier-URL" content="http://sms.hosting-cm.com">
    <meta name="directory" content="submission">
    <meta name="category" content="business">
    <meta name="coverage" content="Worldwide">
    <meta name="distribution" content="Global">
    <meta name="rating" content="General">
    <meta name="revisit-after" content="7 days">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">

    <title>{{ config('app.title', 'Bulk SMS') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/admin/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
</head>
<body>
    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-45868036-3', 'auto');
      ga('send', 'pageview');

    </script>
    
    <div id="id">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.title', 'Welcome') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li>
                                <a href="{{ route('buySmsPlan') }}">
                                    Buy a pack
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('formSMSToSend') }}">
                                    Send SMS
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('confirmSMSPlan') }}">
                                    My SMS Plan
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->full_name }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-default panel-flush">
                        <div class="panel-heading">
                            Available plans
                        </div>
                        <div class="panel-body">
                            <ul class="nav">
                                @foreach($all_plans as $p)
                                <li role="presentation">
                                    <a href="">
                                        <i class="fa fa-caret-right"></i>
                                        {{ $p->plan_name }}<br /><strong style="text-align: right">{{ $p->sms_quantity }} SMS - {{ $p->plan_cost }} XAF</strong>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    
                    <div class="panel panel-default panel-flush">
                        <div class="panel-heading">
                            Subscribed plan
                        </div>
                        <div class="panel-body">
                            <ul class="nav">
                                @foreach($last_plan as $p)
                                <li role="presentation"><a href=""><i class="fa fa-caret-right"></i>{{$p->plan_name}}</a></li>
                                    <strong>Remaining : {{ $p->sms_quantity }}</strong>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    
                    <div class="panel panel-default panel-flush">
                        <div class="panel-heading">
                            Settings
                        </div>
                        <div class="panel-body">
                            <ul class="nav">
                                <li role="presentation"><a href=""><i class="fa fa-user"></i>&nbsp;&nbsp;Profile</a></li>
                                <li role="presentation"><a href=""><i class="fa fa-lock"></i>&nbsp;&nbsp;Security</a></li>
                                <li role="presentation"><a href=""><i class="fa fa-code"></i>&nbsp;&nbsp;APIs</a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="panel panel-default panel-flush">
                        <div class="panel-heading">
                            Billing
                        </div>
                        <div class="panel-body">
                            <ul class="nav">
                                <li role="presentation"><a href=""><i class="fa fa-dollar"></i>&nbsp;&nbsp;Payment methods</a></li>
                                <li role="presentation"><a href="{{ route('invoices') }}"><i class="fa fa-th-list"></i>&nbsp;&nbsp;Invoices</a></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
                @yield('content')
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
