@extends('layouts.template')

@section('content')
<style>
label{
    color: #000;
}
.page_container{
    background-color: #fff;
    border-top: 2px solid #ccc;
    max-width:100%;
    margin-top:0px;
}
</style>
<div class="main">
    <div class="page_container">
        <div id="panel panel-default">
            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-6"><h3>Password forgotten ?</h3><span>Enter your email address and get the email with a link to reset your account</span></div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" style="color: #fff; background-color: #86c724; border-radius: 2px; border:1px solid #86c724; padding: 5px 20px;">
                                Send Password Reset Link
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
