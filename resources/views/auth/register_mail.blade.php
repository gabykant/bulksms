<!doctype html>
<html>
    <head>
        <title>Registration confirmation for </title>
    </head>
<body>
    <header>
        <h3>Hi {!! $user->full_name !!}</h3><hr />
    </header>

    <section>
        <p style="color: #ccc;">
            Thanks for signing up for Bulk SMS Mass Communication. Good news, you have been selected to receive 25 free SMS in your account to try the service! 
            <br />Follow these four quick steps to get started: 

            <ul style="list-style: none">
                <li>
                    <h4>1. Activate your account</h4>
                    <span>Please click or copy and paste the following link to your browser to activate your account</span>
                </li>
                <li>
                    <h4>2. Sign in to the application</h4>
                    <span>To log in to your account, you must provide your username <b>{!! $user->email !!}</b> and the associate password <b>******</b></span>
                </li>
                <li>
                    <h4>3. Send your SMS</h4>
                    <span>Create groups of recipients and import you contact list into it. Then you are ready to send the SMS Campain and convert your new customers</span>
                </li>
            </ul>
        </p>
    </section>
    <br /><br />
    <footer>
        <hr />
        <p>
            © 2017 K Soft Solutions. Boulevard de la Libert&eacute;, Akwa Douala
        </p>
    </footer>
</body>
</html>