@extends('layouts.template')

@section('content')
<style>
label{
    color: #000;
}
.page_container{
    background-color: #fff;
    border-top: 2px solid #ccc;
    max-width:100%;
    margin-top:0px;
}
</style>
<div class="main">
    <div class="page_container">
        <div id="panel panel-default">
            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-6"><h3>Sign Up</h3><span>Create a new account</span></div>
                    </div>
                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                        <label for="username" class="col-md-4 control-label">Username</label>
                        <div class="col-md-6">
                            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>
                            @if ($errors->has('username'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Password</label>
                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                    </div>                    
                    <div class="form-group{{ $errors->has('sender_name') ? ' has-error' : '' }}">
                        <label for="sender_name" class="col-md-4 control-label">Sender Name</label>
                        <div class="col-md-6">
                            <input id="sender_name" type="sender_name" class="form-control" name="sender_name" required>
                            @if ($errors->has('sender_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('sender_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
                        <label for="full_name" class="col-md-4 control-label">full Name</label>
                        <div class="col-md-6">
                            <input id="full_name" type="full_name" class="form-control" name="full_name" required>
                            @if ($errors->has('full_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('full_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                        <label for="phone_number" class="col-md-4 control-label">Phone Number</label>
                        <div class="col-md-6">
                            <input id="phone_number" type="phone_number" class="form-control" name="phone_number" required>
                            @if ($errors->has('phone_number'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" style="color: #fff; background-color: #86c724; border-radius: 2px; border:1px solid #86c724; padding: 5px 20px;">
                                Register Now
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>      
</div>    
@endsection
