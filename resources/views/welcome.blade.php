@extends('layouts.template')

@section('content')
<div class="main">
    <div class="page_container">
        <div id="immersive_slider">
            <div class="slide" data-blurred="">
                <div class="col-md-6 image">
                    <img src="{{ asset('images/33a.jpg') }}" alt="Slider 1" />
                </div>
                <div class="col-md-6 content">
                    <h3>Reach out <span>Your customer</span></h3>
                    <p>In 1-click, spread the world to your customers and enjoy your sales or promote your products</p>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="slide" data-blurred="">
                <div class="col-md-6 image">
                    <img src="{{ asset('images/11a.jpg') }}" alt="Slider 1" />
                </div>
                <div class="col-md-6 content">
                    <h3>Data <span>Analysis</span></h3>
                    <p>Test your products to end users and get back feedback. We drive a compaign and analyse data to help you take decision </p>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="slide" data-blurred="">
                <div class="col-md-6 image">
                    <img src="{{ asset('images/22a.jpeg') }}" alt="Slider 1" />
                </div>
                <div class="col-md-6 content">
                    <h3>We <span>Integrate</span></h3>
                    <p>You lack developers to integrate our solutions to your activities ? Don't worry! We do that for you</p>
                </div>
                <div class="clearfix"> </div>
            </div>
                
            <a href="#" class="is-prev">&laquo;</a>
            <a href="#" class="is-next">&raquo;</a>
        </div>
    </div>
</div>

<div class="banner-bottom">
    <div class="container">
        <div class="w3-banner-bottom-heading">
            <h3>How <span>It Works?</span></h3>
        </div>
        <div class="agileits-banner-bottom">
            <div class="col-md-3 agileits-banner-bottom-grid">
                <div class="services-grid1">
                    <div class="services-grid-right agile-services-grid-right">
                        <div class="services-grid-right-grid hvr-radial-out blue-grid">
                            <span class="glyphicon glyphicon-usd" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="services-grid-left agile-services-grid-left">
                        <h4>Buy a pack</h4>
                        <p>Choose the pack that best fits to your need</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 agileits-banner-bottom-grid">
                <div class="services-grid1">
                    <div class="services-grid-right agile-services-grid-right">
                        <div class="services-grid-right-grid hvr-radial-out orange-grid">
                            <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="services-grid-left agile-services-grid-left">
                        <h4>Prepare your recipients list</h4>
                        <p>Create groups of recipients and import your contacts to each group. Then just select the group to which to send SMS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 agileits-banner-bottom-grid">
                <div class="services-grid1">
                    <div class="services-grid-right agile-services-grid-right">
                        <div class="services-grid-right-grid hvr-radial-out green-grid">
                            <span class="glyphicon glyphicon-message-lock" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="services-grid-left agile-services-grid-left">
                        <h4>Write down your SMS Campaign</h4>
                        <p>What do you really want to send your customers? A new campaign ? A promo code ?</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 agileits-banner-bottom-grid">
                <div class="services-grid1">
                    <div class="services-grid-right agile-services-grid-right">
                        <div class="services-grid-right-grid hvr-radial-out red-grid">
                            <span class="glyphicon glyphicon-transfer" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="services-grid-left agile-services-grid-left">
                        <h4>Push the SMS</h4>
                        <p>Ready to send out your message? Go ! And enjoy</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<style>
    ul.pricings.green > li {
        background: #FFF;
        -webkit-transition: all 0.2s ease-in-out;
        -moz-transition: all 0.2s ease-in-out;
        transition: all 0.2s ease-in-out;
    }
    
    ul.pricings > li {
        width: 24% !important;
        float: left;
        margin-right: 1%;
        position: relative;
        z-index: 5;
    }
</style>
<div class="special">
    <div class="container">
        <div class="w3-banner-bottom-heading">
            <h3>Our special <span>Packages</span></h3>
        </div>
        <div class="wthree-special-grid">
            <div class="flipster1">
                <ul style="list-style: none;padding: 0;" class="pricings green">
                <li style="width: 20%">
                    <div class="pricing">
                        <div class="pricing-top green-top">
                            <h3>Starter</h3>
                            <p>2 500 XAF</p>
                        </div>
                        <div class="pricing-bottom">
                            <div class="pricing-bottom-bottom">
                                <p><span>Unlimited</span> Email Addresses</p>
                                <p><span>65GB </span> of Storage</p>
                                <p><span>75</span> Databases</p>  
                                <p><span>25</span> Domains</p>
                                <p class="text"><span>24/7</span> Unlimited Support</p>
                            </div>
                            <div class="buy-button">
                                <a href="plans.html">Buy Now</a>
                            </div>
                        </div>
                    </div>
                </li>
                <li style="width: 20%">
                    <div class="pricing">
                        <div class="pricing-top blue-top">
                            <h3>Premium</h3>
                            <p>20 000 XAF</p>
                        </div>
                        <div class="pricing-bottom">
                            <div class="pricing-bottom-bottom">
                                <p><span>Unlimited</span> Email Addresses</p>
                                <p><span>85GB </span> of Storage</p>
                                <p><span>65</span> Databases</p>  
                                <p><span>30</span> Domains</p>
                                <p class="text"><span>24/7</span> Unlimited Support</p>
                            </div>
                            <div class="buy-button">
                                <a href="plans.html">Buy Now</a>
                            </div>
                        </div>
                    </div>
                </li>
                <li style="width: 20%">
                    <div class="pricing">
                        <div class="pricing-top">
                            <h3>Business</h3>
                            <p>35 000 XAF</p>
                        </div>
                        <div class="pricing-bottom">
                            <div class="pricing-bottom-bottom">
                                <p><span>Unlimited</span> Email Addresses</p>
                                <p><span>50GB </span> of Storage</p>
                                <p><span>55</span> Databases</p>  
                                <p><span>21</span> Domains</p>
                                <p class="text"><span>24/7</span> Unlimited Support</p>
                            </div>
                            <div class="buy-button">
                                <a href="plans.html">Buy Now</a>
                            </div>
                        </div>
                    </div>
                </li>
                <li style="width: 20%">
                    <div class="pricing">
                        <div class="pricing-top green-top">
                            <h3>Corporate</h3>
                            <p>130 000 XAF</p>
                        </div>
                        <div class="pricing-bottom">
                            <div class="pricing-bottom-bottom">
                                <p><span>SMS</span> 5 000</p>
                                <p><span>Access data analysis </span> Yes</p>
                                <p><span>Scheduling</span> Yes</p>  
                                <p><span>API Integration</span> Yes</p>
                                <p class="text"><span>24/7</span> Unlimited Support</p>
                            </div>
                            <div class="buy-button">
                                <a href="plans.html">Buy Now</a>
                            </div>
                        </div>
                    </div>
                </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="choose jarallax">
    <div class="w3-agile-testimonial">
        <div class="container">
            <div class="w3-agileits-choose">
                <div class="col-md-12 choose-grid">
                    <div class="w3-banner-bottom-heading choose-heading">
                        <h3>They work<span> with Us</span></h3>
                    </div>
                    <div class="top-choose-info">
                        <div class="choose-info-top">
                            <div class="choose-left-grid col-sm-2">
                                <img src="images/customers/ksoftsolutions.jpg" width="80px" />
                            </div>
                            
                            <div class="choose-left-grid col-sm-2">
                                <img src="images/customers/logo-desktop-wesite.png" />
                            </div>
                            <div class="choose-right-grid col-sm-2">
                                <img src="images/customers/ksoftsolutions.jpg" width="80px" />
                            </div>
                            <div class="choose-right-grid col-sm-2">
                                <img src="images/customers/ksoftsolutions.jpg" width="80px" />
                            </div>
                            <div class="choose-right-grid col-sm-2">
                                <img src="images/customers/logo-fbds.png" />
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</div>
<div class="subscribe">
    <div class="container">
        <div class="w3-banner-bottom-heading">
            <h4>Need help? <span> We're always here for you.</span></h4>
        </div>
        <div class="w3-agile-subscribe-form">
            <form action="#" method="post">
                <button class="btn1" style="border-radius: 5px; border-color:#86c724; background: #86c724">Chat with a live person</button>
            </form>
        </div>
    </div>
</div>

@endsection