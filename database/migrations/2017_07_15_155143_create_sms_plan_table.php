<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_plan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plan_name', 200)->unique();
			$table->integer('sms_quantity');
            $table->integer('validity_days');
            $table->integer('plan_cost');
            $table->integer('activate_plan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_plan');
    }
}
