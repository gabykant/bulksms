<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerSmsplanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_smsplan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->comment('The ID of the customer');
            $table->integer('smsplan_id')->comment('The ID of the SMS Plan');
            $table->dateTime('valid_till')->comment('The date of validity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_smsplan');
    }
}
