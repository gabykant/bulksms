<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 200)->unique();
            $table->string('email', 200)->unique();
            $table->string('password', 200);
            $table->string('sender_name', 200)->unique();
            $table->string('full_name', 200);
            $table->string('phone_number', 200);
            $table->integer('account_validation')->default(0);
            $table->string('activation_code', 200)->unique();
            $table->integer('activation_status')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
